@extends ('layout.master')

@section ('title', 'Create Post')

@section ('content')

<section class="content">
  <div class="pl-3">
    <a href="/pertanyaan"><button type="button" class="btn btn-warning mb-3">Back to Index</button></a>
  </div>
  <div class="card card-primary"> 
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/pertanyaan" method="Post">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="judul">Judul</label>
                    <input type="text" class="form-control" id="judul" name="judul" placeholder="Masukkan judul" value="{{old('title', '')}}">
                    @error('judul')
                      <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="isi">Isi</label>
                    <textarea class="form-control" id="isi" name ="isi" placeholder="Isi Post" value="{{old('isi', '')}}" rows="10" cols="30"></textarea>
                    @error('isi')
                      <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="profile_id">Pembuat Post</label>
                    <select id="profile_id" name="profile_id">
                      @foreach($profiles as $key => $value)
                        <option value="{{$value->id}}">{{$value->nama_lengkap}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
  </div>
</section>


@endsection