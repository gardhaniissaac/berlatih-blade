@extends ('layout.master')

@push('css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@endpush

@section ('title', 'Detail')

@section ('content')

<section class="content">
  <div class="card">
    <div class="card-header">
    <div class="card-body mt-2">
      <table class="table table-bordered">
        @foreach($data as $key => $value)
          @foreach($value as $kunci => $nilai)
            <tr>
              <td>{{$kunci}}</td>
              <td>{{$nilai}}</td>
            </tr>
          @endforeach
        @endforeach
      </table>
    </div>
  </div>
  <div class="pl-3 my-2">
    <a href="/pertanyaan"><button type="button" class="btn btn-warning">Back to Index</button></a>
  </div>
</section>


@endsection