<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function index()
    {
    	$post = DB::table('posts')->leftJoin('profiles', 'profiles.id', 'posts.profile_id')->select('posts.*','profiles.nama_lengkap')->get();
        return view('post.index', compact('post'));
    }

    public function create()
    {
    	$profiles = DB::table('profiles')->get();
    	return view('post.form',compact('profiles'));
    }

    public function store(Request $request)
    {
    	$request->validate([
    		"judul" => 'required|unique:posts',
    		"isi"	=> 'required'
    	]);
    	$query = DB::table('posts')->insert([
    		"judul" => $request['judul'],
    		"isi"	=> $request['isi'],
    		"profile_id" => $request["profile_id"]
    	]);

    	return redirect('/pertanyaan')->with('success', 'Post berhasil disimpan');
    }

    public function show($pertanyaan_id)
    {
    	$data = DB::table('posts')->where('posts.id','=' ,$pertanyaan_id)->leftJoin('profiles', 'profiles.id', 'posts.profile_id')->select('posts.id as Id','posts.judul as Judul','posts.isi as Isi Post','profiles.nama_lengkap as Penulis','posts.created_at as Tanggal Dibuat','posts.updated_at as Tanggal Diperbarui')	->get();
    	// dd($data);
    	return view('post.detail', compact('data'));
    }

    public function edit($pertanyaan_id)
    {
    	$data = DB::table('posts')->where('id',$pertanyaan_id)->get()->all();
    	$profiles = DB::table('profiles')->get();
    	$datax = $data[0];
    	
    	return view('post.form_edit',[
    		'data' => $datax,
    		'profiles' => $profiles
    	]);
    }

    public function update($pertanyaan_id, Request $request)
    {
    	$request->validate([
    		"judul" => 'required',
    		"isi"	=> 'required'
    	]);
    	$query = DB::table('posts')->where('id',$pertanyaan_id)->update([
    		"judul" => $request['judul'],
    		"isi"	=> $request['isi'],
    		"profile_id" => $request["profile_id"]
    	]);

    	return redirect('/pertanyaan')->with('success', 'Post berhasil diupdate');
    }

    public function destroy($pertanyaan_id)
    {
    	$data = DB::table('posts')->where('id',$pertanyaan_id)->delete();

    	return redirect('/pertanyaan')->with('success', "Post berhasil dihapus");
    }
}
